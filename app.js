var express = require('express');
var app = express();
var Request = require('request');

// http://localhost:3000/api/weather?zipcode=
app.get('/api/weather', function (req,res) {
  //  to get zip code from params
  var zipcode=req.query.zipcode;
	// api to get country code from zip code
	Request.get('http://maps.googleapis.com/maps/api/geocode/json?address='+zipcode+'&sensor=true', function(error, response, body){
		var location = JSON.parse(body)
		// to get the country code
		var locationCode = location.results[0].address_components[location.results[0].address_components.length-1].short_name
		// openweathermap api to get the weather information
		Request.get('http://api.openweathermap.org/data/2.5/weather?zip='+zipcode+','+locationCode+'&appid=9ea3144cda0d58b5d8a1b7bcd7afed3c', function(error, response, body){
		 	var temp = JSON.parse(body)
		 	// weather detail object
		 	var weather={
		 		Description: temp.weather.description ? temp.weather.description :'' ,
		 		Temp:temp.main.temp ? temp.main.temp :'' ,
		 		Humidity:temp.main.humidity ? temp.main.humidity :'' ,
		 		Wind_speed: temp.wind.speed ? temp.wind.speed :'' ,
		 		Name_of_the_city: temp.name ? temp.name :'' 
		 	}
		 	res.send(weather)
		})
	})
});

app.listen(3000, function(){
	console.log('server running at 3000');
})
